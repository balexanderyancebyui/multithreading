package com.balexanderyancebyui.cit360;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/********************************************
 * Class: Namer
 * An object from this class is responsible for
 * giving sequential names to the future
 * "Thread" objects.
 */
class Namer {

    //This variable will hold what iteration
    //the Namer is on.
    int i;

    //Constructor. Each namer gets its own iterator.
    Namer() {
        this.i = 0;
    }

    /*****************************************
     * Method: name
     * This method gives a name to an object.
     * Specifically, it gives sequential numbered
     * names to each thread object.
     *
     * It is `synchronized` so the object cannot
     * accidentally be accessed out of order.
     */
    synchronized String name() {
        return "Thread" + this.i++;
    }
}

/*************************************************
 * Class: Spammer
 * Custom Runnable class instead of in-line
 */
class Spammer implements Runnable {

    /*******************************************
     * Constructor
     * All this does is start the thread.
     * Thread.start(target, name) will begin the thread the same way
     * ExecutorService.execute(runnable) does.
     */
    public Spammer(Namer namer) {
        /////////////////(this,(get a name)).start()
        new Thread(this, namer.name()).start();
    }

    /********************************************
     * run() gives the runnable what it will do when
     * it is started
     */
    public void run() {

        //Literally the same as what happens below.
        System.out.println(Thread.currentThread().getName() + " started.");
        try {
            //Sleep the thread for 6 seconds
            Thread.sleep(6000);
        } catch (InterruptedException e) { e.printStackTrace(); }

        System.out.println(Thread.currentThread().getName() + " finished.");
    }
}


public class  Main {

    public static void main(String[] args) throws InterruptedException {

        //Create a Namer object so we can give correct names
        //to each thread.
        Namer namer = new Namer();

        //Create a Random object so we can make the thread
        //sleep for a random time.
        Random ran = new Random();

        //Create a new threadpool named `taskmaster`
        ExecutorService taskmaster = Executors.newFixedThreadPool(10);

        //Create a runnable object using a lambda expression.
        //This runnable can be executed as many times as we have threads.
        Runnable runner = () -> {
            //Print to the console when a new thread has started, by name.
            System.out.println(Thread.currentThread().getName() + " started");
            try {
                //Once the thread begins, make it sleep for 2.5 to 5 seconds.
                Thread.sleep(5000 - ran.nextInt(2500));
            } catch (InterruptedException e) {
                //If the thread is interrupted ...
                e.printStackTrace();
            }
            //Print to the console when the thread has finished.
            System.out.println(Thread.currentThread().getName() + " finished");
        };

        //Create twenty threads.
        for (int i = 0; i < 10; i++) {
            //Create a runnable via the Spammer class.
            new Spammer(namer);
            //Create a runnable via the taskmaster ExecutorService.
            taskmaster.execute(runner);

            //Wait 1/4 second before going again.
            Thread.sleep(250);
        }

        taskmaster.shutdown();
    }
}
